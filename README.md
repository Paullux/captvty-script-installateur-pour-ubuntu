# Captvty Installateur Pour Linux Ubuntu  
  
Il s'agit de deux scripts bash pour l'installation de [Captvty V2 (stable)](https://captvty.fr/) ou [Captvty V3 (alpha)](https://v3.captvty.fr/) sous Ubuntu Linux.  
Ce programme développé pour windows permet de regarder la tv en direct ou de récupérer les programmes tv depuis le replay.  
Du coup, les scripts installent wine et configure des prefix wine pour faire tourner la version du logiciel choisit.  
Attention pour la V3 le script installe WineHQ Staging donc utilise un dépôt tiers, mais la V2 est pleinement fonctionnel avec la version de Wine des dépots d'Ubuntu.  
Le logiciel s'installe dans le dossier utilisateur, du coup si vous voulez l'utiliser pour tous les utilisateurs de votre système, il faut l'exécuter sur chaque session.  
  
Les scripts créent des raccourcis dans votre menu d'application, quelque soit votre DE (kde, xfce, gnome, etc.), du coup vous pouvez avoir les deux versions en parallèles.  
Les scripts génèrent les paramètres suivant : lecteur vlc (version wine) comme lecteur de média par défaut pour le visionnage de la tv et l'enregistrement des vidéos dans le dossier ~/Vidéos/Captvty ou ~/Vidéos/Captvty_V3 selon la version.  
  
Pour info, si jamais vous avez un message vous indiquant une mise à jour du logiciel, vous n'avez qu'à réxécuter le script et le logiciel sera automatiquement mis à jour.  
  
## Installation  
  
Pour installer Captvty V2 veuillez suivre les commandes suivantes:  
  
```bash
wget https://framagit.org/Paullux/captvty-script-installateur-pour-ubuntu/raw/master/CaptvtyV2.sh
chmod +x CaptvtyV2.sh
./CaptvtyV2.sh
```  
  
Pour installer Captvty V3 veuillez suivre les commandes suivantes:  
  
```bash
wget https://framagit.org/Paullux/captvty-script-installateur-pour-ubuntu/raw/master/CaptvtyV3.sh
chmod +x CaptvtyV3.sh
./CaptvtyV3.sh
```  
  
**Il y a une chose à noter pour la version 3, il faut décocher la mise à jour automatique de VLC, sinon il devient impossible de regarder le direct et les vidéos de rattrapage sans les télécharger complétement.**   
  
  Une fois un script lancé, laissez le tourner (pas besoin d'intéraction supplémentaire), et une fois l'exécution du script terminée allez dans votre menu et Captvty sera présent.  
  
## Contributing  
  
Pour la création des scripts, j'ai eu l'aide de la communauté [Ubuntu-fr.org](https://forum.ubuntu-fr.org/viewtopic.php?id=2027322)


## Test Complet de Captvty V3 sous Ubuntu 18.10 avec WineHQ-Staging :

[![Watch the video](./ImageVideoYoutube.png)](https://youtu.be/a5CEMoGBehA)  
