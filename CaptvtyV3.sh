#!/bin/bash

#Choix de la version de Wine et installation des paquets nécessaires selon le choix 
VersionWine=$(whiptail --title "Version de Wine" --radiolist \
"Choix de la version de wine ?" 15 106 3 \
"Version hq" "Nécessite l'ajout d'un dépôt tiers (meilleurs performance)" ON \
"Version des dépots d'Ubuntu" " Problème pour voir la tv en direct" OFF \
"Garder ma configuration actuelle" "Risque de ne pas marcher" OFF \
3>&1 1>&2 2>&3)
 
exitstatus=$?
if test $exitstatus -eq 0
then
    if test "$VersionWine" = "Version hq" 
    then
        sudo dpkg --add-architecture i386 
        wget -nc https://dl.winehq.org/wine-builds/winehq.key
        sudo apt-key add winehq.key
        sudo apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/'
        sudo apt update && sudo apt upgrade -y
        sudo apt install -y winehq-staging wine-staging winetricks
        rm winehq.key 
        #Création du prefix wine pour Captvty
        export WINEPREFIX="$HOME/.wine_captvty_V3"
        export WINEARCH=win64
        wineboot -u
    elif test "$VersionWine" = "Version des dépots d'Ubuntu"
    then
        sudo dpkg --add-architecture i386
        sudo apt install -y wine32 wine winetricks
        #Création du prefix wine pour Captvty
        export WINEPREFIX="$HOME/.wine_captvty_V3"
        export WINEARCH=win32
        wineboot -u
    else
    #Création du prefix wine pour Captvty
    export WINEPREFIX="$HOME/.wine_captvty_V3"
    wineboot -u
    fi
else
    echo "Vous avez annulé"
    exit 0
fi

#Ajout sur le prefix des éléments nécessaires à Captvty
wineboot --init

#Désinstallation de mono
wine uninstaller --remove '{E45D8920-A758-4088-B6C6-31DBB276992E}'
wine64 uninstaller --remove '{E45D8920-A758-4088-B6C6-31DBB276992E}'

#Installation des différentes bibliothèque pour faire tourner le programme

winetricks -q dotnet452 corefonts gdiplus fontsmooth=rgb vlc win7

#Remise à zéro du dossier où est contenu les fichiers du logiciel en cas de mise à jour
test -d "$HOME/.captvty_V3" && rm -rf "$HOME/.captvty_V3"
#(Re-)Création du dossier pour le logiciel
mkdir "$HOME/.captvty_V3"

#Récupération du programme proprement dit
adresse=$(wget -q -O- 'http://v3.captvty.fr' | sed -n 's/.*href="\(\/\/.\+\.zip\).*/http:\1/p')
test -n "$adresse" && wget -qO /tmp/Captvty.zip "$adresse"
if test -n /tmp/Captvty.zip
then
   unzip -d "$HOME/.captvty_V3/" /tmp/Captvty.zip &&  rm /tmp/Captvty.zip
fi

#Récupération de l'icône
test -d "$HOME/.icons" || mkdir "$HOME/.icons"
wget "https://framagit.org/Paullux/captvty-script-installateur-pour-ubuntu/raw/master/captvty-logo.png" -O "$HOME/.icons/captvty-logo.png"

#Création des préférences de Captvty
cat <<FIN > "$HOME/.captvty_V3/Captvty.settings"
<?xml version="1.0" encoding="utf-8"?>
<settings>
  <maxRateFactor>5</maxRateFactor>
  <playerPaths>C:/Program Files (x86)/VideoLAN/VLC/vlc.exe</playerPaths>
  <maxRateEnabled>False</maxRateEnabled>
  <remuxEnabled>True</remuxEnabled>
  <downloadLocation>Z:/$HOME/Vidéos/Captvty_V3</downloadLocation>
  <maxJobs>0</maxJobs>
  <remuxRecycleEnabled>False</remuxRecycleEnabled>
  <recordingPaddingEnd>0</recordingPaddingEnd>
  <bandwidth>0</bandwidth>
  <windowMetrics>0, 0, 0, 0</windowMetrics>
  <remuxFormats>mp4</remuxFormats>
  <maxJobsEnabled>False</maxJobsEnabled>
  <recordingPaddingStart>0</recordingPaddingStart>
  <recordingPaddingEnabled>False</recordingPaddingEnabled>
  <windowState></windowState>
</settings>
FIN

#Création du fichier desktop pour avoir un raccourci du logiciel dans le menu
test -d "$HOME/.local/share/applications" || mkdir "$HOME/.local/share/applications"
cat << FIN > "$HOME/.local/share/applications/Captvty_V3.desktop"
[Desktop Entry]
Comment[fr_FR]=
Comment=
Exec=env WINEPREFIX="$HOME/.wine_captvty_V3" wine $HOME/.captvty_V3/Captvty.exe
GenericName[fr_FR]=Regarder et enregistrer la tv
GenericName=Regarder et enregistrer la tv
Icon=$HOME/.icons/captvty-logo.png
MimeType=
Name[fr_FR]=Captvty V3
Name=Captvty V3
Path=$HOME
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-KDE-SubstituteUID=false
X-KDE-Username=
FIN
