#!/bin/bash

#Installation des paquets nécessaires
sudo apt install -y wine32 wine-stable winetricks

#Création du prefix wine pour Captvty
export WINEPREFIX="$HOME/.wine_captvty"
export WINEARCH=win32

#Ajout sur le prefix des éléments nécessaires à Captvty
winetricks -q dotnet40 fontsmooth=rgb ie8 vlc
wget http://captvty.fr/getgdiplus -O kb975337.exe
wine kb975337.exe /x:kb975337 /q
cp "kb975337/asms/10/msft/windows/gdiplus/gdiplus.dll" "$HOME/.wine_captvty/drive_c/windows/system32"
wine reg add HKCU\\Software\\Wine\\DllOverrides /v gdiplus /d native,builtin /f
wget http://captvty.fr/getflash -O fplayer.exe
wine fplayer.exe -install -au 2

#Remise à zéro du dossier où est contenu les fichiers du logiciel (en cas de mise à jour)
test -d "$HOME/.captvty" && rm -rf "$HOME/.captvty"
#(Re-)Création du dossier pour le logiciel
mkdir "$HOME/.captvty"

#Récupération du programme proprement dit
adresse=$(wget -q -O- 'http://captvty.fr' | sed -n 's/.*href="\(\/\/.\+\.zip\).*/http:\1/p')
test -n "$adresse" && wget -qO /tmp/Captvty.zip "$adresse"
if test -n /tmp/Captvty.zip
then
   unzip -d "$HOME/.captvty/" /tmp/Captvty.zip &&  rm /tmp/Captvty.zip
fi

#Effacement des fichiers qui ne sont plus nécessaire
rm -Rf kb975337.exe fplayer.exe kb975337/ 
#Récupération de l'icône
test -d "$HOME/.icons" || mkdir "$HOME/.icons"
wget "https://framagit.org/Paullux/captvty-script-installateur-pour-ubuntu/raw/master/captvty-logo.png" -O "$HOME/.icons/captvty-logo.png"

#Création des préférences de Captvty, iconv sert pour la conversion des caractères accentués linux vers windows (Captvty étant un programme windows)
cat << FIN > "$HOME/.captvty/captvty1.ini"
[General]
SkuPriority=1
DownloadDir=Z:\\home\\$USER\\Vidéos\\Captvty
Metrics=0:510:222:900:589
[Players]
0=C:\\Program Files\\VideoLAN\\VLC\\vlc.exe
1=Builtin
2=Website
FIN
iconv -f UTF-8 -t ISO-8859-1 "$HOME/.captvty/captvty1.ini" > "$HOME/.captvty/captvty.ini"
rm -f "$HOME/.captvty/captvty1.ini"

#Création du fichier desktop pour avoir un raccourci du logiciel dans le menu
test -d "$HOME/.local/share/applications" || mkdir "$HOME/.local/share/applications"
cat << FIN > "$HOME/.local/share/applications/Captvty.desktop"
[Desktop Entry]
Comment[fr_FR]=
Comment=
Exec=env WINEPREFIX="$HOME/.wine_captvty" wine $HOME/.captvty/Captvty.exe
GenericName[fr_FR]=Regarder et enregistrer la tv
GenericName=Regarder et enregistrer la tv
Icon=$HOME/.icons/captvty-logo.png
MimeType=
Name[fr_FR]=Captvty
Name=Captvty
Path=$HOME
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-KDE-SubstituteUID=false
X-KDE-Username=
FIN
